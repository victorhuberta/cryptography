from lib.bch import BCHCode
from lib.bchutils import print_matrix

print_matrix(BCHCode(6, 2).gmatrix)
print_matrix(BCHCode(10, 6).gmatrix)
print_matrix(BCHCode(16, 12).gmatrix)

from fractions import Fraction
import operator as op

from .bcherrors import GaussJordanError, FractionModuloError
from .bchutils import transpose_matrix, _lv_arith, _ll_arith

class GMatrixSolver(object):
    """Find/solve a generator matrix."""

    def __init__(self, out_len, in_len, pchkmatrix):
        """Create a new object of GMatrixSolver.

        Note: further information on positional arguments
        can be found in bch module.

        Positional arguments:
        out_len -- BCH code output length
        in_len -- BCH code input length
        pchkmatrix -- BCH code parity check matrix
        
        Returns:
        solver -- new object of GMatrixSolver
        """
        self.out_len = out_len
        self.in_len = in_len
        self.boundary = self.out_len + 1
        self.pchkmatrix = pchkmatrix


    def solve(self):
        """Find a BCH code generator matrix from a given parity check matrix.

        The solution is to utilize Gauss-Jordan algorithm for reducing an area
        of the parity check matrix to Reduced Row Echelon Form while ensuring
        all arithmetic operations are done on all items and under a modulus.

        ==Examples for BCHCode(10, 6)==
        After reduction, a matrix of coefficients is created:
        | a1 b1 c1 d1 e1 f1 1 0 0 0 | R1
        | a2 b2 c2 d2 e2 f2 0 1 0 0 | R2
        | a3 b3 c3 d3 e3 f3 0 0 1 0 | R3
        | a4 b4 c4 d4 e4 f4 0 0 0 1 | R4
        where a - f are all base coefficients for creating generator matrix.

        The next few steps are simple. Final coefficients matrix
        can be derived by:
        R1' = (-1 * R1) % boundary
        R2' = (-1 * R2) % boundary
        R3' = (-1 * R3) % boundary
        R4' = (-1 * R4) % boundary
        where coeff_matrix = transpose([R1', R2', R3', R4'])

        So, the generator matrix will be:
        | 1 0 0 0 0 0 + coeff_matrix[0] |
        | 0 1 0 0 0 0 + coeff_matrix[1] |
        | 0 0 1 0 0 0 + coeff_matrix[2] |
        | 0 0 0 1 0 0 + coeff_matrix[3] |
        | 0 0 0 0 1 0 + coeff_matrix[4] |
        | 0 0 0 0 0 1 + coeff_matrix[5] |

        Returns:
        gmatrix -- generator matrix
        """
        # Convert all items to fractions.
        fm = [list(map(Fraction.from_decimal, row)) for row in self.pchkmatrix]

        # Use Gauss-Jordan algorithm to get reduced form of matrix.
        result = self.gauss_jordan(fm, self.out_len - self.in_len)

        # Convert all items back to decimals.
        fm = [list(map(lambda item: item.numerator, row)) for row in fm]

        # Create coefficients matrix to find generator matrix.
        coeff_matrix = []
        for row in fm:
            new_row = _lv_arith(op.mul, row[:self.in_len],
                -1, fraction=False, mod=self.boundary)
            coeff_matrix.append(new_row)

        coeff_matrix = transpose_matrix(coeff_matrix)

        # Create generator matrix from coefficients matrix.
        gmatrix = [[1 if i == j else 0 for j in range(self.in_len)]
            for i in range(self.in_len)]

        for i in range(len(gmatrix)):
            gmatrix[i].extend(coeff_matrix[i])

        return gmatrix


    def _select_reduced_area(self, fm, start_col):
        """Select an area of matrix for Gauss-Jordan reduction.

        Positional arguments:
        fm -- full matrix
        start_col -- starting matrix column index

        Returns:
        m -- a smaller area of original matrix
        """
        return [row[-start_col:] for row in fm]


    def gauss_jordan(self, fm, start_col, eps=1.0/(10**10)):
        """Puts given matrix (2D array) into the Reduced Row Echelon Form.

        ==Authorship==
        Original code: https://elonen.iki.fi/code/misc-notes/python-gaussj/
        Written by Jarno Elonen in April 2005, released into Public Domain.

        Modified for BCH code usage by Victor Huberta in December 2016.
        ===
         
        Operations will be done on the whole matrix, although Reduced
        Row Echelon Form will only be enforced on specific area of matrix.
        This functionality is used to find correct coefficients for
        generator matrix from a given parity check matrix.

        More information on Gauss-Jordan algorithm can be found here:
        https://en.wikipedia.org/wiki/Gaussian_elimination

        Positional arguments:
        fm -- whole matrix to be operated on
        start_col -- starting column index of matrix to be reduced

        Keyword arguments:
        eps -- epsilon, or numerical tolerance to check for singular matrix

        Returns:
        rm -- reduced matrix

        Raises:
        GaussJordanError
        """

        # Select reduced area of matrix.
        # Note: this will be done after every modification of fm.
        m = self._select_reduced_area(fm, start_col)

        # Get height and width of reduced area.
        (h, w) = (len(m), len(m[0]))

        for y in range(0,h):
            # Find max row and switch it with current row.
            maxrow = y
            for y2 in range(y+1, h):
                if abs(m[y2][y]) > abs(m[maxrow][y]):
                    maxrow = y2
            (fm[y], fm[maxrow]) = (fm[maxrow], fm[y])
            m = self._select_reduced_area(fm, start_col)

            # Check if matrix is singular.
            if abs(m[y][y]) <= eps:
                raise GaussJordanError('Matrix is singular')

            # Eliminate column y.
            for y2 in range(y+1, h):
                c = m[y2][y] / m[y][y]
                row = _lv_arith(op.mul, fm[y], c, mod=self.boundary)
                fm[y2] = _ll_arith(op.sub, fm[y2], row, mod=self.boundary)
                m = self._select_reduced_area(fm, start_col)

        # Back substitute the rest (now that we've zeroed out the bottom left).
        for y in range(h-1, 0-1, -1):
            c  = m[y][y]
            for y2 in range(0,y):
                row = _lv_arith(op.mul, fm[y], m[y2][y] / c, mod=self.boundary)
                fm[y2] = _ll_arith(op.sub, fm[y2], row, mod=self.boundary)
                m = self._select_reduced_area(fm, start_col)

            fm[y] = _lv_arith(op.truediv, fm[y], c, mod=self.boundary)
            m = self._select_reduced_area(fm, start_col)

        return fm

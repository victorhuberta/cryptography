class InvalidBCHCode(Exception):
    pass


class UnusableNumberError(Exception):
    pass


class NoInverseError(Exception):
    pass


class NoSquareRootModuloError(Exception):
    pass


class NoPossibleCorrectionError(Exception):
    pass


class FractionModuloError(Exception):
    pass


class GaussJordanError(Exception):
    pass

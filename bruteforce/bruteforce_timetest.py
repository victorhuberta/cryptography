import hashlib
import sys
from lib.ptspec import AsciiPTSpec
from lib.ptgenerator import PrintablePTGenerator

spec = AsciiPTSpec(0, 10, alphabetic=False, numeric=True)
ptg = PrintablePTGenerator(spec)
target_hash = 'b6589fc6ab0dc82cf12099d1c2d40ab994e8410c'
algo = hashlib.sha1

def find_pass(target_hash, algo, ptlist):
    for pt in ptlist:
        digest = algo(pt.encode('utf-8')).hexdigest()
        if digest == target_hash:
            print('Found: %s -> %s' % (pt, digest))
            sys.exit(0)

for ptlist in ptg.generate():
    find_pass(target_hash, algo, ptlist)

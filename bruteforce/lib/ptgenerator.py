from abc import ABCMeta, abstractmethod
from .ptspec import CharsetMismatchError
from bchcode.lib.bch import BCHCode
from bchcode.lib.bcherrors import UnusableNumberError

class PTGenerator(metaclass=ABCMeta):
    """Base class for all kinds of plain text generator."""

    def __init__(self, ptspec):
        """Create a new object of PTGenerator.

        Positional arguments:
        ptspec -- object of PTSpec

        Returns:
        ptgen -- new object of PTGenerator
        """
        self.charset = ptspec.get_charset()
        self.minlen = ptspec.get_minlen()
        self.maxlen = ptspec.get_maxlen()


    def generate(self):
        """Generate plain text lists based on lengths from minlen to maxlen.

        Yields:
        ptlist -- list of plain texts with the specified length
        """
        plaintext = ''
        for curlen in range(self.minlen, self.maxlen + 1):
            yield self.mkptlist(curlen)

    
    @abstractmethod
    def mkptlist(self, curlen):
        """Generate plain texts.

        Positional arguments:
        curlen -- the current specified length

        Yields:
        plaintext -- plain text with the specified length
        """
        pass


class PrintablePTGenerator(PTGenerator):
    """Plain text generator for printable plain texts only."""

    def mkptlist(self, curlen):
        """Generate printable plain texts.

        Look at PTGenerator.mkptlist(...) to read more description.
        """
        plaintext = self.charset[0] * curlen
        ptspace = len(self.charset) ** curlen # plain text space

        for _ in range(ptspace):
            yield plaintext
            # Increment the plain text by 1 shift
            plaintext = ''.join(
                self.incpt(list(plaintext), charset=self.charset))


    def incpt(self, alist, *, charset):
        """Increment the last character of a list based on charset.

        From right to left, increment char of list and if its result
        surpasses the last char of charset, increment the next char
        in list. If it reaches the beginning of list, prepend the
        first char from charset to list.

        Positional arguments:
        alist -- list of chars

        Keyword arguments:
        charset -- set of possible characters

        Returns:
        ilist -- incremented list of chars
        """
        if charset == '': return []
        if alist == []: return [charset[0]]

        # Loop it in reverse.
        for i in range(len(alist) - 1, -1, -1):
            try:
                newcharpos = charset.index(alist[i]) + 1
            except ValueError:
                raise CharsetMismatchError('There\'s a char \
                    which doesn\'t belong to the charset.')

            if newcharpos < len(charset):
                alist[i] = charset[newcharpos]
                return alist
            else:
                alist[i] = charset[0]

        return [charset[0]] + alist


#    def _incpt(self, alist, *, charset):
#        """A recursion alternative to incpt(...).
#
#        This function isn't used because it causes the stack to
#        quickly build up as the plain texts get longer.
#        """
#        if charset == '': return []
#        if alist == []: return [charset[0]]
#
#        lastind = len(alist) - 1
#        try:
#            newlastind = charset.index(alist[lastind]) + 1
#        except ValueError:
#            raise CharsetMismatchError('There\'s a char \
#                which doesn\'t belong to the charset.')
#
#        if newlastind < len(charset):
#            alist[lastind] = charset[newlastind]
#            return alist
#        else:
#            alist[lastind] = charset[0]
#            return self.incpt(alist[:-1], charset=charset) + [alist[lastind]] 
    

class BCH106PTGenerator(PTGenerator):
    """Wrapper class for PTGenerator to support BCHCode(10, 6) plain texts."""

    def __init__(self, ptgenerator):
        """Create a new object of BCH106PTGenerator.

        Positional arguments:
        ptgenerator -- wrapped PTGenerator object

        Returns: new object of BCH106PTGenerator
        """
        self.ptgenerator = ptgenerator
        self.bchcode = BCHCode(10, 6)


    def generate(self):
        """Generate only one plain text list with length 6.

        Look at PTGenerator.generate(...) to read more description.
        """
        yield self.mkptlist(6)


    def mkptlist(self, curlen):
        """Generate BCHCode(10, 6) plain texts.

        Look at PTGenerator.mkptlist(...) to read more description.
        """
        for pt in self.ptgenerator.mkptlist(6):
            try:
                yield self.bchcode.encode(pt)
            except UnusableNumberError:
                pass # We don't need to do anything here.

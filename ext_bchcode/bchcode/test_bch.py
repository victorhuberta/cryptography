import unittest
from testhelper import printfunc, logcall
from lib.bch import *
from lib.bcherrors import *
from lib.bchutils import *

class BCHCodeTestCase(unittest.TestCase):
    """Test case for BCHCode(10, 6) object."""

    def setUp(self):
        self.bchcode = BCHCode(10, 6)
    

    @printfunc
    def test_inverse(self):
        args = {1: 1, 2: 6, 3: 4, 4: 3, 5: 9,
            6: 2, 7: 8, 8: 7, 9: 5, 10: 10}
        faileds = [0, 11]

        for arg, expected in args.items():
            logcall(self, arg, expected, inverse, arg, mod=11)

        for failed in faileds:
            logcall(self, failed, NoInverseError, inverse, failed,
                mod=11, should_raise=True)


    @printfunc
    def test_sqrtmod(self):
        args = {1: 1, 3: 5, 4: 2, 5: 4, 9: 3}
        faileds = [2, 6, 7, 8, 10]
        
        for arg, expected in args.items():
            logcall(self, arg, expected, sqrtmod, arg, mod=11)

        for failed in faileds:
            logcall(self, failed, NoSquareRootModuloError,
                sqrtmod, failed, mod=11, should_raise=True)


    @printfunc
    def test__gen_pchkmatrix(self):
        matrix = self.bchcode._gen_pchkmatrix()
        logcall(self, None,
            [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
             [1, 2, 3, 4, 5, 6, 7, 8, 9,10],
             [1, 4, 9, 5, 3, 3, 5, 9, 4, 1],
             [1, 8, 5, 9, 4, 7, 2, 6, 3,10]],
            self.bchcode._gen_pchkmatrix,
        )


    @printfunc
    def test__gen_genmatrix(self):
        pchkmatrix = self.bchcode._gen_pchkmatrix()
        logcall(self, None,
            [[1, 0, 0, 0, 0, 0, 4, 7, 9, 1],
             [0, 1, 0, 0, 0, 0,10, 8, 1, 2],
             [0, 0, 1, 0, 0, 0, 9, 7, 7, 9],
             [0, 0, 0, 1, 0, 0, 2, 1, 8,10],
             [0, 0, 0, 0, 1, 0, 1, 9, 7, 4],
             [0, 0, 0, 0, 0, 1, 7, 6, 7, 1]],
            self.bchcode._gen_genmatrix,
            pchkmatrix
        )

    
    @printfunc
    def test__mult_matrix_digits(self):
        args = {'0000118435': [0, 0, 0, 0], '8899880747': [2, 7, 3, 3]}
        matrix = self.bchcode._gen_pchkmatrix()

        for arg, expected in args.items():
            digits = [int(c) for c in arg]
            logcall(self, arg, expected, self.bchcode._mult_matrix_digits,
                matrix, digits) 


    @printfunc
    def test_encode(self):
        args = {'000001': '0000017671', '000002': '0000023132',
            '000010': '0000101974', '000011': '0000118435'}

        for arg, expected in args.items():
            logcall(self, arg, expected, self.bchcode.encode, arg)

        faileds = ['000003', '000009']

        for failed in faileds:
            logcall(self, failed, UnusableNumberError, self.bchcode.encode,
                failed, should_raise=True)

    @printfunc
    def test__correct_str(self):
        args = {
            ('3945195876', 2, 2): '3745195876',
            ('3745995876', 5, 8): '3745195876',
        }

        for arg, expected in args.items():
            logcall(self, arg, expected, self.bchcode._correct_str, *arg)


    @printfunc
    def test_decode(self):
        args = {
            '3745195876': '3745195876', '3945195876': '3745195876',
            '3715195076': '3745195876', '0743195876': '3745195876',
            '3745195840': '3745195876', '8745105876': '3745195876',
            '3745102876': '3745195876', '1145195876': '3745195876',
            '3745191976': '3745195876', '3745190872': '3745195876',
        }

        for arg, expected in args.items():
            logcall(self, arg, expected, self.bchcode.decode, arg)

        faileds = ['2745795878', '3742102896', '1115195876',
            '3121195876', '1135694766', '0888888074', '5614216009',
            '9990909923', '1836703776', '9885980731']

        for failed in faileds:
            logcall(self, failed, NoPossibleCorrectionError,
                self.bchcode.decode, failed, should_raise=True)


if __name__ == '__main__':
    unittest.main()

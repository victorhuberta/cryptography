from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from checkcc import CreditCard, CreditCardError, str_to_digits

def check_validity(ccno):
    """Check validity of user input credit card numbers.

    Display an error dialog if an exception is thrown.
    Display an info dialog instead to show validity.

    Positional arguments:
    ccno -- user input credit card numbers (UI text var)

    Returns: None
    """
    try:
        cc = CreditCard(str_to_digits(ccno.get()))
        if cc.is_valid():
            messagebox.showinfo(message='It is valid!')
        else:
            messagebox.showinfo(message='It is invalid!')
    except CreditCardError as err:
        messagebox.showerror(message=str(err))


def main():
    root = Tk()
    root.title('Credit Card Checker')

    mainframe = ttk.Frame(root, padding='3 3 12 12')
    mainframe.grid(row=0, column=0, sticky=(N, W, E, S))
    mainframe.rowconfigure(0, weight=1)
    mainframe.columnconfigure(0, weight=1)

    # Credit card numbers UI text variable
    ccno = StringVar()

    # Credit card numbers label and entry
    ttk.Label(mainframe, text='Credit Card Number').grid(
        row=0, column=0, sticky=(W, E))
    ccno_entry = ttk.Entry(mainframe, width=20, textvariable=ccno)
    ccno_entry.grid(row=1, column=0, sticky=(W, E))
    ccno_entry.focus()

    # Check validity button
    ttk.Button(mainframe, text='Check Validity',
        command=lambda : check_validity(ccno)).grid(
            row=2, column=0, sticky=E)

    for child in mainframe.winfo_children():
        child.grid_configure(padx=5, pady=5)
    
    root.bind('<Return>', lambda e: check_validity(ccno))
    root.mainloop()


if __name__ == '__main__':
    main()

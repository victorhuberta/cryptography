import unittest
from testhelper import printfunc, logcall
from checkcc import CreditCard, CreditCardError, str_to_digits

class CreditCardTestCase(unittest.TestCase):
    """Test case for CreditCard class."""

    @printfunc
    def test___init__(self):
        faileds = [
            '41111111111111112222',
            '550000000',
            '34000000000000911',
            '300000000000040',
        ]

        for failed in faileds:
            logcall(self, failed, CreditCardError,
                CreditCard, str_to_digits(failed), should_raise=True)


    @printfunc
    def test_is_valid(self):
        args = {
            '4111111111111111': True,
            '5500000000000004': True,
            '3400000000000091': True,
            '3000000000000400': True,
            '4111211111111111': False,
            '5500500000000004': False,
            '3400000033000091': False,
            '3000000009020400': False,
        }

        def is_cc_valid(ccno):
            return CreditCard(str_to_digits(ccno)).is_valid()
        
        for arg, expected in args.items():
            logcall(self, arg, expected, is_cc_valid, arg)


if __name__ == '__main__':
    unittest.main()

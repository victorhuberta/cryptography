def str_to_digits(s):
    """Convert a string to a list of digits.

    Positional arguments:
    s -- string to be converted

    Returns:
    digits -- a list of digits
    """
    return list(map(lambda x: int(x), s))


class CreditCardError(Exception):
    pass


class CreditCard(object):
    """Contain credit-card-related functions."""

    def __init__(self, digits):
        """Create a new CreditCard object after validation.

        Raise an error if length of digits is not 16.

        Positional arguments:
        digits -- credit card numbers

        Returns:
        credit_card -- a new CreditCard object

        Raises:
        CreditCardError
        """
        if len(digits) != 16:
            raise CreditCardError('Credit card number must have length 16')

        self.digits = digits


    def is_valid(self):
        """Check the validity of credit card numbers.

        To learn more about Luhn algorithm, visit:
        https://en.wikipedia.org/wiki/Luhn_algorithm

        Returns:
        True -- credit card numbers is valid
            OR
        False -- credit card numbers is invalid
        """
        digits = self.digits.copy()

        for i in range(0, len(self.digits), 2):
            newdigit = digits[i] * 2
            newdigit = newdigit - (9 if newdigit >= 10 else 0)
            digits[i] = newdigit
        
        return (sum(digits) % 10 == 0)

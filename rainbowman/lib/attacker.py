import gzip
import pickle
from .chain import Chain

class RBTAttacker(object):
    """Attack a target hash with a stored rainbow table."""

    def __init__(self, chain_maker, target_hash):
        """Create a new object of RBTAttacker.

        Positional arguments:
        chain_maker -- object of ChainMaker for producing chains
        target_hash -- target hash to attack

        Returns:
        attacker -- new object of RBTAttacker
        """
        self.chain_maker = chain_maker
        self.target_hash = target_hash
        self.rbtable = dict()
        self.max_clen = 1


    def parse_file(self, infile):
        """Read an input rainbow table file.

        The file is first being uncompressed (GZIP), then it's unpickled
        (Python object deserialization).

        Positional arguments:
        infile -- rainbow table file name

        Returns: None
        """
        with gzip.open(infile, 'rb') as f:
            pickled = f.read()
            self.rbtable = pickle.loads(pickled)

            # Find the longest chain length in rainbow table.
            self.max_clen = max(map(lambda item: item[1].clen,
                self.rbtable.items()))


    def attack(self):
        """Find matching chain and find matching plain text afterwards.

        Returns:
        plaintext -- matching plain text in a matching chain
        """
        found_chain = None

        # Plain text is assumed to be located at ptpos.
        for ptpos in range(self.max_clen):
            if found_chain != None: break

            # We need start_rfno because we have X number of reduce functions.
            start_rfno = ptpos % len(self.chain_maker.reductor.reducfuncs)
            found_chain = find_chain(start_rfno)

        return self.find_plaintext(found_chain)


    def find_chain(self, start_chainpos):
        """Find which chain has the plain text.

        For length N chains, reductions must be performed for at least
        N times. start_chainpos is the starting position in the chain
        at which it should start reducing plain texts (used for
        multi-processing).

        Each reduced result is compared with chains' ending plain
        texts to find which chain has the plain text (implemented
        as hash table indexing).

        Positional arguments:
        start_chainpos -- starting position in the chain for reduction

        Returns:
        chain -- chain which contains the plain text
        """
        reductor = self.chain_maker.reductor
        digest = self.target_hash

        for chainpos in range(start_chainpos, self.max_clen):
            rfno = chainpos % len(reductor.reducfuncs)
            plaintext = reductor.reduce(rfno, digest)
            digest = self.chain_maker.hash(plaintext)

        return self.rbtable.get(plaintext)

    
    def find_plaintext(self, chain):
        """Find plain text in a chain.

        Positional arguments:
        chain -- chain which contains the plain text

        Returns:
        plaintext -- found plain text
        """
        if chain == None: return None

        reductor = self.chain_maker.reductor
        plaintext = chain.startpt

        for chainpos in range(chain.clen):
            rfno = chainpos % len(reductor.reducfuncs)
            digest = self.chain_maker.hash(plaintext)
            if digest == self.target_hash:
                return plaintext
            plaintext = reductor.reduce(rfno, digest)

        return None

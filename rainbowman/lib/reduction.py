import random
from .primo import next_prime

class Reductor(object):
    """Perform reduction of a hash to a plain text."""

    def __init__(self, reducseed, ptspec, algo, n_reducfuncs):
        """Create a new object of Reductor.

        Most of the required parameters for reduction are
        initialized here.

        Positional arguments:
        reducseed -- seed for generated reduce functions
        ptspec -- plain text specification (AsciiPTSpec)
        algo -- hash algorithm to use
        n_reducfuncs -- number of generated reduce functions

        Returns:
        rd -- new object of Reductor
        """
        self.reducseed = reducseed
        self.ptlen = ptspec.get_maxlen()
        self.charset = ptspec.get_charset()
        self.ptspace = self._calc_ptspace()
        
        # P is the next prime number larger than ptspace.
        self.P = next_prime(self.ptspace)

        # Calculate reduce coefficients.
        self.rcoefficients = self._calc_rcoefficients(algo)

        # Generate all reduce functions.
        self.reducfuncs = self._gen_reducfuncs(n_reducfuncs)
        self.cur_rfno = 0 # reduce function number starts from 0.


    def _calc_ptspace(self):
        """Calculate total plain text space for length 0 to ptlen.

        Returns:
        ptspace -- total number of possible plain texts
        """
        ptspace = 0
        for curlen in range(self.ptlen + 1):
            ptspace += (len(self.charset) ** curlen)
        return ptspace


    def _calc_rcoefficients(self, algo):
        """Calculate reduce coefficients/weights.

        Reduce coefficients is a sequence of numbers used as weights
        when summing ascii codes of hash. The weights are calculated
        as follows:
        let k = length of charset,
            c = a char of hash,
            n = sum of ascii codes with weights.
        then
            n = c0 * k^0 + c1 * k^1 + c2 * k^2 + ...
            _calc_rcoefficients(...) == [k^0, k^1, k^2, ...]

        Positional arguments:
        algo -- hash algorithm to use

        Returns:
        rcoefficients -- reduce coefficients
        """
        rcoefficients = [1]

        # digest string has twice the size of actual digest.
        diglen = algo().digest_size * 2

        for i in range(1, diglen):
            rcoefficients.append(len(self.charset) * rcoefficients[i - 1])

        return rcoefficients
    

    def _gen_reducfuncs(self, n_reducfuncs):
        """Generate reduce functions based on seed.

        Positional arguments:
        n_reducfuncs -- number of reduce functions

        Returns:
        reducfuncs -- list of reduce functions
        """
        random.seed(self.reducseed)
        reducfuncs = []
        for _ in range(n_reducfuncs):
            reducfuncs.append(
                self._reducfunc(
                    int(random.random() * len(self.charset)) + 1))
        return reducfuncs


    def nextreduce(self, digest):
        """Perform reduction using the next reduce function in list.

        Positional arguments:
        digest -- hash string to reduce

        Returns:
        plaintext -- reduced result
        """
        if self.cur_rfno >= len(self.reducfuncs):
            self.cur_rfno = 0 # reset rfno

        plaintext = self.reducfuncs[self.cur_rfno](digest)
        self.cur_rfno += 1

        return plaintext


    def reduce(self, rfno, digest):
        """Reduce a digest with a specific reduce function.

        Positional arguments:
        rfno -- reduce function index number
        digest -- hash string to reduce

        Returns:
        plaintext -- reduced result
        """
        return self.reducfuncs[rfno](digest)


    def set_modifier_at(self, rfno, modifier):
        """Modify the number of modifier in a specific reduce function.

        Positional arguments:
        rfno -- reduce function index number
        modifier -- reduce function modifier

        Returns: None
        """
        self.reducfuncs[rfno] = self._reducfunc(modifier)


    def start_at(self, rfno):
        """Set the starting reduce function index.

        Positional arguments:
        rfno -- reduce function index

        Returns: None
        """
        if rfno < len(self.reducfuncs):
            self.cur_rfno = rfno
        else:
            self.cur_rfno = 0


    def _reducfunc(self, modifier):
        """Produce a reduce function with embedded modifier.

        Positional arguments:
        modifier -- a modifier value used by the reduce function

        Returns:
        reducfunc -- reduce function
        """
        def reducfunc(digest):
            # Find weighted sum by multiplying reduce coefficients
            # to ascii values of hash's characters.
            ascii_weighted_sum = sum([ord(c) * coeff for c, coeff in
                zip(digest, self.rcoefficients)])

            # Wrap *modified* hash sum space into a smaller plain text space.
            bigint = (ascii_weighted_sum * modifier) % self.P

            return self.to_plaintext(bigint, self.ptlen)

        return reducfunc
    

    def to_plaintext(self, bigint, maxlen):
        """Convert big integer into plain text (length 0 to maxlen).

        Positional arguments:
        bigint -- big integer to be converted
        maxlen -- maximum length of plain text

        Returns:
        plaintext -- plain text derived from bigint
            properties: len(plaintext) <= maxlen
        """
        base = len(self.charset)
        plaintext = ''

        while bigint > 0:
            r = bigint % base
            plaintext = self.charset[r] + plaintext
            if len(plaintext) >= maxlen:
                break

            bigint = bigint // base

            # To make the distribution of lengths suits
            # the ratio of plain text spaces.
            bigint -= 1

        return plaintext

import string
from abc import ABCMeta, abstractmethod

class PTSpec(metaclass=ABCMeta):
    """Base class of all kinds of plain text specification.

    All text specifications must contain:
    charset -- a set of possible characters
    minlen -- minimum length of plain text
    maxlen -- maximum length of plain text
    """

    @abstractmethod
    def get_charset(self): pass


    @abstractmethod
    def get_minlen(self): pass


    @abstractmethod
    def get_maxlen(self): pass


class AsciiPTSpec(PTSpec):
    """PTSpec implementation for ASCII plain texts."""

    def __init__(self, minlen, maxlen, *,
        numeric=False, alphabetic=False,
        uppercase=False, lowercase=True):

        """Create a new specification for ASCII plain texts.

        Positional arguments:
        minlen -- minimum length of plain text
        maxlen -- maximum length of plain text
        
        Keyword arguments:
        numeric -- should charset contain numbers? (default False)
        alphabetic -- should charset contain alphabets? (default False)
        uppercase -- should charset contain uppercase alphabets?
            (default False)
        lowercase -- should charset contain lowercase alphabets?
            (default True)

        Returns:
        ptspec -- a new object of AsciiPTSpec

        Raises:
        InvalidCharsetError
        """
        if not numeric and not alphabetic:
            raise InvalidCharsetError('At least numeric or alphabetic required')

        self.charset = ''

        if alphabetic and lowercase:
            self.charset += string.ascii_lowercase
        if alphabetic and uppercase:
            self.charset += string.ascii_uppercase
        if numeric:
            self.charset += string.digits

        self.minlen = minlen
        self.maxlen = maxlen


    def get_charset(self):
        return self.charset
    

    def get_minlen(self):
        return self.minlen

    
    def get_maxlen(self):
        return self.maxlen


class InvalidCharsetError(Exception):
    """Thrown when charset is NOT possible to be created."""
    pass


class CharsetMismatchError(Exception):
    """Thrown when a character read is not in charset."""
    pass

from tkinter import *
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
import hashlib
import threading

from lib.ptspec import AsciiPTSpec
from lib.reduction import Reductor
from lib.chain import ChainMaker
from lib.builder import RBTBuilder
from lib.attacker import RBTAttacker
from rainbowman_mp import build_rbtable_fromtofile, mp_attack

root = Tk()
root.title('Rainbowman')

# UI variables for plain text specification
algorithm = StringVar()
chainlen = IntVar()
reducseed = DoubleVar()
max_ptlen = IntVar()
is_alphabetic = BooleanVar()
is_numeric = BooleanVar()

# UI variables for building rainbow table
ptfilename = StringVar()
rbtable_name = StringVar()

# UI variables for attacking a digest with rainbow table
rbtfilename = StringVar()
target_hash = StringVar()

# For reporting any job status.
job_status = StringVar()

# Status of any job,
# used by validate_spec_then(), build_rbtable(), and attack().
in_progress = False

# Status of build job,
# used by build_rbtable() and update_chains_count().
build_done = False

def open_ptfile():
    """Open a file dialog and get a plain text file name."""
    ptfilename.set(filedialog.askopenfilename())


def validate_spec_then(task):
    """Validate plain text specification and run a task.

    Positional arguments:
    task -- task to be run

    Returns: None
    """
    global in_progress

    if not is_alphabetic.get() and not is_numeric.get():
        messagebox.showerror(message='Select at least one criterion')
        return

    in_progress = True
    try:
        configure_local_widgets(enabled=False)

        # Run the task in a new thread.
        threading.Thread(target=task, daemon=True).start()
    except Exception as err:
        messagebox.showerror(message=str(err))


def retrieve_algorithm():
    """Get the user-given hash algorithm function."""
    if algorithm.get() == 'SHA-1':
        return hashlib.sha1
    elif algorithm.get() == 'MD5':
        return hashlib.md5
    else:
        return None


def retrieve_cmaker():
    """Setup a chain maker for building and attacking purposes.

    A ChainMaker object needs:
        - length of chain
        - hash algorithm function
        - Reductor object, which in turn needs:
            * reduction seed
            * plain text specification
            * hash algorithm function
            * length of chain

    Returns:
    cmaker -- ChainMaker object
    """
    algo = retrieve_algorithm()
    if algo == None:
        messagebox.showerror(message='Selected hash algorithm not supported')
        return None

    ptspec = AsciiPTSpec(max_ptlen.get(), max_ptlen.get(), alphabetic=
        is_alphabetic.get(), numeric=is_numeric.get())
    reductor = Reductor(reducseed.get(), ptspec, algo, chainlen.get())
    cmaker = ChainMaker(chainlen.get(), algo, reductor)

    return cmaker


def build_rbtable():
    """Glue the GUI with rainbowman_mp.build_rbtable_fromtofile() function."""
    global in_progress
    global build_done

    # Create a ChainMaker object.
    cmaker = retrieve_cmaker()
    if cmaker == None: return

    # Create a RBTBuilder object.
    builder = RBTBuilder(cmaker)

    # Start a thread to update GUI of build progress.
    update_status_thread = threading.Thread(target=update_chains_count,
        args=(builder,), daemon=True)
    update_status_thread.start()

    # Build rainbow table using the created builder.
    build_rbtable_fromtofile(builder, ptfilename.get(),
        get_full_rbtable_name())

    # Report various statistics on the build for QA.
    builder._report_mean_duplication()
    builder._report_mean_distribution()

    build_done = True
    update_status_thread.join()
    in_progress = False


def update_chains_count(builder):
    """Update GUI of rainbow table build progress.

    Positional arguments:
    builder -- RBTBuilder object to get update info from

    Returns: None
    """
    global build_done

    while True:
        if build_done:
            job_status.set('Done! Rainbow table saved as \'%s\'.'
                % get_full_rbtable_name())
            build_done = False
            return
        else:
            job_status.set('Building... %d chains.' % len(builder.rbtable))


def get_full_rbtable_name():
    """Get the full name of built or loaded rainbow table."""
    return rbtable_name.get() + '.rbt'


def open_rbtfile():
    """Open a file dialog and get a rainbow table file name."""
    rbtfilename.set(filedialog.askopenfilename())


def attack():
    """Glue the GUI with rainbowman_mp.mp_attack() function."""
    global in_progress

    # Create a ChainMaker object.
    cmaker = retrieve_cmaker()
    if cmaker == None: return

    # Create a RBTAttacker object and parse plain text file.
    attacker = RBTAttacker(cmaker, target_hash.get())
    job_status.set('Parsing file...')
    attacker.parse_file(rbtfilename.get())

    # Start attacking target hash.
    job_status.set('Attacking %s... Please wait.' % target_hash.get())
    plaintext = mp_attack(attacker)

    # Update GUI depending on whether or not plain text has been found.
    if plaintext == None:
        job_status.set('Plain text not found :(')
    else:
        job_status.set('Found :) Plain text is \'%s\'.' % plaintext)
    in_progress = False


def get_local_frames():
    """Return a list of GUI frames.

    The frames object are used in configure_local_widgets(...).

    Returns:
    frames -- list of affected GUI frames
    """
    return [spec_frame, builder_frame, attacker_frame]


def configure_local_widgets(*, enabled):
    """Configure the state of widgets.

    In common scenarios, it's used to enable and disable widgets.

    Keyword arguments:
    enabled -- enabled state of widgets

    Returns: None
    """
    frames = get_local_frames()
    for frame in frames:
        for child in frame.winfo_children():
            if enabled:
                child.config(state='normal')
            else:
                child.config(state='disabled')


def refresh_fields():
    """Blank/clear all the form fields if no work's in progress."""
    if in_progress:
        messagebox.showinfo(message='It\'s still in progress!')
        return
    configure_local_widgets(enabled=True)
    job_status.set('No jobs yet.')


# Setup main frame.
mainframe = ttk.Frame(root, padding='3 3 12 12')
mainframe.grid(row=0, column=0, sticky=(N, E, S, W))
mainframe.rowconfigure(0, weight=1)
mainframe.columnconfigure(0, weight=1)

# Button for clearing form fields
newjob_btn = ttk.Button(mainframe, text='New Job', command=refresh_fields)
newjob_btn.grid(row=0, column=0, sticky=E)


##############################################################################
#
# Frame for plain text specification
spec_frame = ttk.Labelframe(mainframe, text='Rainbow Table Specification')
spec_frame.grid(row=1, column=0, sticky=(N, W, E, S))

# Label and combo box for hash algorithms
ttk.Label(spec_frame, text='Hash Algorithm').grid(row=0, column=0, sticky=(W, E))
algo_cmbbox = ttk.Combobox(spec_frame, width=25, textvariable=algorithm,
    state='readonly')
algo_cmbbox['values'] = ('SHA-1', 'MD5')
algo_cmbbox.current(0)
algo_cmbbox.grid(row=0, column=1, sticky=(W, E))

# Label and spin box for chain length
ttk.Label(spec_frame, text='Chain Length').grid(row=1,
    column=0, sticky=(W, E))
chainlen_spin = Spinbox(spec_frame, width=6, from_=1,
    to=10000, textvariable=chainlen)
chainlen_spin.grid(row=1, column=1, sticky=W)

# Label and text entry for reduction seed
ttk.Label(spec_frame, text='Reduction Seed').grid(row=2, column=0,
    sticky=(W, E))
reducseed_entry = ttk.Entry(spec_frame, width=25, textvariable=reducseed)
reducseed_entry.grid(row=2, column=1, sticky=(W, E))

# Label and spin box for plain text max length
ttk.Label(spec_frame, text='Plain Text Max Length').grid(row=3, column=0,
    sticky=(W, E))
max_ptlen_spin = Spinbox(spec_frame, width=3, from_=1,
    to=48, textvariable=max_ptlen)
max_ptlen_spin.grid(row=3, column=1, sticky=W)

# Label and check buttons for plain text categories
# (alphabetic and/or numeric)
ttk.Label(spec_frame, text='Plain Text Categories').grid(row=4, column=0,
    sticky=(W, E))
alphabets_chkbtn = ttk.Checkbutton(spec_frame, text='Alphabets',
    variable=is_alphabetic, onvalue=True, offvalue=False)
alphabets_chkbtn.grid(row=4, column=1, sticky=W)
numbers_chkbtn = ttk.Checkbutton(spec_frame, text='Numbers',
    variable=is_numeric, onvalue=True, offvalue=False)
numbers_chkbtn.grid(row=4, column=1, sticky=E)


##############################################################################
#
# Frame for building rainbow table
builder_frame = ttk.Labelframe(mainframe, text='Build Rainbow Table')
builder_frame.grid(row=2, column=0, sticky=(N, W, E, S))

# Label and button for loading plain text file
ttk.Label(builder_frame, textvariable=ptfilename).grid(row=0, column=0,
    sticky=(W, E), columnspan=10)
ldptfile_btn = ttk.Button(builder_frame, text='Load Plain Text File...',
    command=open_ptfile)
ldptfile_btn.grid(row=1, column=0, sticky=(W, E), columnspan=2)

# Label and text entry for rainbow table name
ttk.Label(builder_frame, text='Table Name').grid(row=2, column=0, sticky=(W, E))
rbtname_entry = ttk.Entry(builder_frame, width=40, textvariable=rbtable_name)
rbtname_entry.grid(row=2, column=1, sticky=(W, E))

# Button for building rainbow table
build_btn = ttk.Button(builder_frame, text='Build', command=
    lambda : validate_spec_then(build_rbtable))
build_btn.grid(row=3, column=1, sticky=E)


##############################################################################
#
# Frame for attacking target hash with rainbow table
attacker_frame = ttk.Labelframe(mainframe, text='Attack Using Rainbow Table')
attacker_frame.grid(row=3, column=0, sticky=(N, W, E, S))

# Label and button for loading rainbow table file
ttk.Label(attacker_frame, textvariable=rbtfilename).grid(row=0, column=0,
    sticky=(W, E), columnspan=10)
ldrbtfile_btn = ttk.Button(attacker_frame, text='Load Rainbow Table File...',
    command=open_rbtfile)
ldrbtfile_btn.grid(row=1, column=0, sticky=(W, E), columnspan=2)

# Label and text entry for target hash
ttk.Label(attacker_frame, text='Target Hash').grid(row=2, column=0,
    sticky=(W, E))
targethash_entry = ttk.Entry(attacker_frame, width=40, textvariable=target_hash)
targethash_entry.grid(row=2, column=1, sticky=(W, E))

# Button for attacking target hash
attack_btn = ttk.Button(attacker_frame, text='Attack', command=
    lambda : validate_spec_then(attack))
attack_btn.grid(row=3, column=1, sticky=E)

# Label for reporting any job progress
ttk.Label(mainframe, textvariable=job_status).grid(row=4, column=0, sticky=(W, E))


for frame in [mainframe, spec_frame, builder_frame, attacker_frame]:
    for child in frame.winfo_children():
        child.grid_configure(padx=5, pady=5)

root.mainloop()
